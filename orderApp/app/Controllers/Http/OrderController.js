'use strict'

class OrderController {
    async create ({ request, response }) {
        const { validate } = use('Validator')
        const Config = use('Config')
        const rules = {
            product_id: 'required|string',
            price: 'required|number'
        }
        const payload = request.all();
        const validation = await validate(payload, rules)
        const PaymentService = use('App/Services/Payment')
        const paymentService = new PaymentService();
        let paid, order;

        if (validation.fails()) {
            return { success: false, error: validation.messages() }
        }

        order = await this.newOrder(payload.product_id, payload.price);

        paid = await paymentService.checkOut({
            orderId: order.id.toString(),
            amount: order.price
        });

        paid ? order.confirm() : order.cancel()
        await order.save()

        setTimeout(this.deliverOrder, Config.get('app.deliver.duration'), order);

        return { success: true, order }
    }

    async cancel ({ request, response }) {
        const { validate } = use('Validator')
        const Order = use('App/Models/Order')
        const rules = {
            order_id: 'required|integer',
        }
        const payload = request.all();
        const validation = await validate(request.all(), rules)
        let order;

        if (validation.fails()) {
            return { success: false, error: validation.messages() }
        }

        order = await Order.find(payload.order_id);
        order.cancel()
        await order.save()

        return { success: true, order }
    }

    async check ({ params }) {
        const { validate } = use('Validator')
        const Order = use('App/Models/Order')

        let order = await Order.find(params.id);

        return { success: true, order }
    }

    async newOrder (product_id, price) {
        const Order = use('App/Models/Order')
        const order = new Order()

        order.product_id = product_id;
        order.price = price;

        await order.save();

        return order;

    }

    deliverOrder(order) {
        order.deliver();
        order.save();
    }
}

module.exports = OrderController
