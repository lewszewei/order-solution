'use strict'

class Payment {
    async checkOut (payload) {
        const Axios = require('axios');
        const Config = use('Config')

        const paymentUrl = Config.get('app.payment.url')
        let paid = false;
        let paymentResult;

        try {
            paymentResult = await Axios.post(
                paymentUrl,
                payload,
                {
                    headers: {'Authorization': 'Bearer abcd1234'}
                }
            );
            console.log(paymentResult.data);
            if (paymentResult.data.success) {
                paid = true;
            }
        } catch (e) {
            console.log(e)
        }

        return paid;
    }
}

module.exports = Payment
