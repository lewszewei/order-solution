'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Order extends Model {
    cancel() {
        this.state = 'Canceled'
    }

    confirm() {
        this.state = 'Confirmed'
    }

    deliver() {
        if (this.state == 'Confirmed') {
            this.state = 'Delivered'
        }
    }
}

module.exports = Order
