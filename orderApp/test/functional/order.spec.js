'use strict'

const { test, trait } = use('Test/Suite')('Order')
const Order = use('App/Models/Order')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')

test('create a confirm order with correct input', async ({ client }) => {
    const PaymentService = require('../utils/PaymentConfirm')

    ioc.fake('App/Services/Payment', () => {
        return PaymentService
    })

    const response = await client
        .post('/order/create')
        .send({
            "product_id": "A1",
            "price": 101.50
        })
        .end()

    response.assertStatus(200)
    response.assertJSONSubset({
        "success": true,
        "order": {
            "product_id": "A1",
            "price": 101.5,
            "state": "Confirmed"
        }
    })

    ioc.restore('App/Services/Payment');

})


test('create a confirm order with wrong price input', async ({ client }) => {
    const PaymentService = require('../utils/PaymentConfirm')

    ioc.fake('App/Services/Payment', () => {
        return PaymentService
    })

    const response = await client
        .post('/order/create')
        .send({
            "product_id": "A1",
            "price": "asdad"
        })
        .end()

    response.assertStatus(200)
    response.assertError({
        success: false,
        error: [{
            message: "number validation failed on price",
            field: "price",
            validation: "number"
        }]
    })

    ioc.restore('App/Services/Payment');

})

test('create a confirm order with wrong product input', async ({ client }) => {
    const PaymentService = require('../utils/PaymentConfirm')

    ioc.fake('App/Services/Payment', () => {
        return PaymentService
    })

    const response = await client
        .post('/order/create')
        .send({
            "product_id": 0,
            "price": 100
        })
        .end()

    response.assertStatus(200)
    response.assertError({
        success: false,
        error: [{
            message: "string validation failed on product_id",
            field: "product_id",
            validation: "string"
        }]
    })

    ioc.restore('App/Services/Payment');

})

test('create a confirm order with empty input', async ({ client }) => {
    const PaymentService = require('../utils/PaymentConfirm')

    ioc.fake('App/Services/Payment', () => {
        return PaymentService
    })

    const response = await client
        .post('/order/create')
        .send({})
        .end()

    response.assertStatus(200)
    response.assertError({
        success: false,
        error: [{
            message: "required validation failed on product_id",
            field: "product_id",
            validation: "required"
        }]
    })

    ioc.restore('App/Services/Payment');

})

test('create a cancel order with correct input', async ({ client }) => {
    const PaymentService = require('../utils/PaymentCancel')

    ioc.fake('App/Services/Payment', () => {
        return PaymentService
    })

    const response = await client
        .post('/order/create')
        .send({
            "product_id": "A1",
            "price": 101.50
        })
        .end()

    response.assertStatus(200)
    response.assertJSONSubset({
        "success": true,
        "order": {
            "product_id": "A1",
            "price": 101.5,
            "state": "Canceled"
        }
    })

    ioc.restore('App/Services/Payment');

})

test('check a exsiting order with correct input', async ({ client }) => {
    const response = await client
        .get('/order/check/1')
        .end()

    response.assertStatus(200)
    response.assertJSONSubset({
        "success": true,
        "order": {
            "id": 1,
            "product_id": "A1",
            "price": 101.5,
            "state": "Confirmed"
        }
    })
})

test('cancel a exsiting order with correct input', async ({ client }) => {
    const response = await client
        .post('/order/cancel')
        .send({
            "order_id": 1,
        })
        .end()

    response.assertStatus(200)
    response.assertJSONSubset({
        "success": true,
        "order": {
            "id": 1,
            "product_id": "A1",
            "price": 101.5,
            "state": "Canceled"
        }
    })
})

test('cancel a exsiting order with wrong input', async ({ client }) => {
    const response = await client
        .post('/order/cancel')
        .send({
            "order_id": 'as',
        })
        .end()

    response.assertStatus(200)
    response.assertError({
        success: false,
        error: [{
            message: "integer validation failed on order_id",
            field: "order_id",
            validation: "integer"
        }]
    })
})

test('deliver a order', async ({ client }) => {
    const PaymentService = require('../utils/PaymentConfirm')
    const sleep = require('sleep');
    const Config = use('Config')

    ioc.fake('App/Services/Payment', () => {
        return PaymentService
    })

    let response = await client
        .post('/order/create')
        .send({
            "product_id": "A1",
            "price": 101.50
        })
        .end()

    await sleep.msleep(Config.get('app.deliver.duration'));

    response = await client
        .get('/order/check/3')
        .end()

    response.assertStatus(200)
    response.assertJSONSubset({
        "success": true,
        "order": {
            "id": 3,
            "product_id": "A1",
            "price": 101.5,
            "state": "Delivered"
        }
    })
})




