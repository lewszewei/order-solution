'use strict'

class PaymentCancel {
    async checkOut (payload) {
        return false;
    }
}

module.exports = PaymentCancel
