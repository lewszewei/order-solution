# order solution applications

This repo contain 2 applications

1. orderApp - to create, cancel and check orders
2. paymentApp - to make payment for the orders

## Setup

1. manually clone the repo and go into orderApp directory and then run `npm install`.
2. copy .env.example as .env if file don't exist.
3. setup database in .env file, recommended using mysql as below:
   ```js
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_USER=<your username>
   DB_PASSWORD=<your password>
   DB_DATABASE=adonis
   ```
4. to run test, recommend using another database and override in .env.testing as below
   ```js
   DB_CONNECTION=mysql
   DB_DATABASE=adonis_test
   ```
5. create both adonis and adonis_test database in your mysql server
6. run the following command to run startup migrations.

```js
node ace test
```


### Start Application

Run the following command to run start both application
```js
npm start
```

### Run Test

Run the following command to run test both application
```js
npm test