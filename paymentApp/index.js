'use strict';

const Hapi = require('hapi');
const AuthBearer = require('hapi-auth-bearer-token');
const Joi = require('joi');

const start = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    await server.register(AuthBearer)

    server.auth.strategy('token', 'bearer-access-token', {
        allowQueryToken: true,
        validate: async (request, token, h) => {

            const isValid = token === 'abcd1234';

            const credentials = { token };
            const artifacts = { test: 'info' };

            return { isValid, credentials, artifacts };
        }
    });


    server.auth.default('token');

    server.route({
        method: 'POST',
        path: '/pay',
        handler: async function (request, h) {
            const orderId = request.payload.orderId;
            const amount = request.payload.amount;
            const ranNum = Math.floor(Math.random() * 10);
            const success = ((ranNum % 2) === 0);

            return {success, orderId, amount}
        },
        options: {
            validate: {
                payload: {
                    orderId: Joi.string().required(),
                    amount: Joi.number().min(0).precision(2).required()
                }
            }
        }
    });

    await server.start();
    console.log('Server running on %ss', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

start();
